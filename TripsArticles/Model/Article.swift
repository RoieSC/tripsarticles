//
//  Article.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import Foundation

struct Article: Decodable {
    var metaData: MetaData
    var id: String
    var title: String
    var imageUrl: String
    var isSaved: Bool
    var isLiked: Bool
    var likesCount: Int
    var category: String
    var author: Author
}

struct MetaData: Decodable {
    var creationTime: Date
    var updateTime: Date
}

struct Author: Decodable {
    var id: String
    var authorName: String
    var authorAvatar: AuthorAvatar
    
    struct AuthorAvatar: Decodable {
        var imageUrl: String
    }
}

struct Articles: Decodable {
    var data: [Article]
}
