//
//  NetworkHelper.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import Foundation

class NetworkHelper {
    
    public class func fetchData<T: Decodable>(urlStr: String, jsonDecoder: JSONDecoder? = nil, completion: @escaping (T?, Error?) -> ()) {
        guard let url = URL(string: urlStr) else {
            completion(nil, nil)
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("NetworkHelper Error: \(error.localizedDescription)")
                completion(nil, error)
                return
            }
            if let data = data {
                let decoder = jsonDecoder != nil ? jsonDecoder : JSONDecoder()
                if let decodedResponse = try? decoder!.decode(T.self, from: data) {
                    completion(decodedResponse, nil)
                } else {
                    completion(nil, nil)
                }
            } else {
                completion(nil, nil)
            }
        }.resume()
    }
    
}
