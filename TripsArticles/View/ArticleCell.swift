//
//  ArticleCell.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import UIKit
import Kingfisher

class ArticleCell: UITableViewCell {

    @IBOutlet weak var articleIV: UIImageView!
    @IBOutlet weak var avatarIV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var updateTimeLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!

    func setArticle(article: Article) {
        titleLabel.text = article.title
        categoryLabel.text = article.category
        authorNameLabel.text = article.author.authorName
        updateTimeLabel.text = article.metaData.updateTime.updateDateString()
        saveButton.setImage(UIImage(named: article.isSaved ? "saved" : "save"), for: .normal)
        likeButton.setImage(UIImage(named: article.isLiked ? "liked" : "like"), for: .normal)
        fetchImages(article: article)
    }

    func fetchImages(article: Article) {
        if let validImageURL = article.imageUrl.validURL() {
            articleIV.kf.indicatorType = .activity
            articleIV.kf.setImage(with: .network(validImageURL))
        }
        if let validAvatarURL = article.author.authorAvatar.imageUrl.validURL() {
            let processor = RoundCornerImageProcessor(cornerRadius: avatarIV.frame.height)
            avatarIV.kf.setImage(with: .network(validAvatarURL), options: [.processor(processor)])
        }
    }
}
