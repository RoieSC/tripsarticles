//
//  ArticlesController.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import UIKit

class ArticlesController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let articlesManager = ArticlesManager()

    override func viewDidLoad() {
        super.viewDidLoad()
//        addCustomBackButton()
        getArticles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getArticles() {
        articlesManager.getArticles { [weak self] (success, error) in
            DispatchQueue.main.async {
                if success {
                    print("count = \(self?.articlesManager.articles.count ?? 0)")
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    deinit {
        print("ArticlesController deinit")
    }
}

extension ArticlesController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articlesManager.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleCell
        let article = articlesManager.articles[indexPath.row]
        cell.setArticle(article: article)
        return cell
    }
}

extension ArticlesController {
    func addCustomBackButton() {
        self.navigationItem.hidesBackButton = true
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(backPressed(sender:)), for: .touchUpInside)
        button.setTitle(" Back", for: .normal)
        button.setTitleColor(UIColor.systemBlue, for: .normal)
        button.setImage(UIImage(named: "backButton"), for: .normal)
        button.setImage(UIImage(named: "backButton"), for: .highlighted)
        button.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func backPressed(sender: UIBarButtonItem) {
//        articlesManager.clearArticles()
//        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
}
