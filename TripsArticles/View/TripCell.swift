//
//  TripCell.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import UIKit

class ArticleCell: UITableViewCell {

    var article: Article?
    @IBOutlet weak var tripIV: UIImageView!
    @IBOutlet weak var avatarIV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCell(article: Article) {
        
    }

}
