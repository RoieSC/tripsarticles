//
//  ArticlesManager.swift
//  TripsArticles
//
//  Created by Roie Shimon Cohen on 29/10/2020.
//

import Foundation

class ArticlesManager {
    
//    static let shared = ArticlesManager()
    private(set) var articles = [Article]()
    
    let articlesUrlStr = "https://cdn.theculturetrip.com/home-assignment/response.json"
        
    func getArticles(completion: @escaping (Bool, Error?)->()) {
        guard let urlStr = articlesUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion(false, nil)
            return
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(ArticlesManager.metaDataDateFormatter())
        NetworkHelper.fetchData(urlStr: urlStr, jsonDecoder: decoder) { [weak self] (articles: Articles?, error) in
            if let error = error {
                print("ArticlesManager getArticles Error: \(error.localizedDescription)")
                completion(false, error)
                return
            }
            if let articles = articles?.data {
                self?.articles = articles
                completion(true, error)
            } else {
                completion(false, error)
            }
        }
    }
    
    deinit {
        print("ArticlesManager deinit")
    }
    
//    func clearArticles() {
//        articles = []
//    }
}


extension ArticlesManager {
    private static func metaDataDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter
    }
}

extension Date {
    func updateDateString() -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM, yyyy"
        return formatter.string(from: self)
    }
}

extension String {
    func validURL() -> URL? {
        if let urlStr = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL(string: urlStr) {
                return url
            }
        }
        return nil
    }
}
